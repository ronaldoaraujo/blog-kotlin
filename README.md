# Importar o Projeto no eclipse

1- Clique com o botão direito do mouse sobre a área do Project Explorer e selecione Import => Import como na imagem abaixo:

![Image1](doc/Screen Shot 2019-02-01 at 08.36.24.png)

2- Expanda a pasta Git e selecione Projects from Git.

![Image2](doc/Screen Shot 2019-02-01 at 08.37.27.png)

3- Na etapa seguinte escolha a opção Clone URI.

![Image3](doc/Screen Shot 2019-02-01 at 08.37.40.png)

4- Digite a URL do Projeto em URI.

![Image4](doc/Screen Shot 2019-02-01 at 08.38.21.png)

5- Na proxima etapa selecione a branch.

![Image5](doc/Screen Shot 2019-02-01 at 08.38.43.png)

6- Escolha um local para o destino do projeto.

![Image6](doc/Screen Shot 2019-02-01 at 08.40.00.png)

7- Escolha a opção Import as general project

![Image7](doc/Screen Shot 2019-02-01 at 08.40.20.png)

8- Digite um nome para o Projeto

![Image8](doc/Screen Shot 2019-02-01 at 08.40.28.png)

9- O Resultado é o abaixo:

![Image9](doc/Screen Shot 2019-02-01 at 08.41.05.png)

10- Para corrigir e transformar em um projeto maven clique novamente com o botão direito e escolha Configure => Convert to Maven Project.
 
![Image10](doc/Screen Shot 2019-02-01 at 08.41.20.png)

11- Pronto! Se seguiu corretamente todos os passos a aparência do Projeto deve ser esta:

![Image10](doc/Screen Shot 2019-02-01 at 08.46.30.png)

 